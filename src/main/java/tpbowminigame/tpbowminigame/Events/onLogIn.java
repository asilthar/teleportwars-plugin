package tpbowminigame.tpbowminigame.Events;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import tpbowminigame.tpbowminigame.Utils.GUImanager;
import tpbowminigame.tpbowminigame.Utils.clearInventoryClass;
import tpbowminigame.tpbowminigame.Utils.loginItem;

public class onLogIn implements Listener {
    @EventHandler(priority = EventPriority.HIGH)
    public void onLogInEvent(PlayerJoinEvent e){
        Player p = e.getPlayer();
        World world = p.getWorld();
        Location spawn = new Location(world, 341, 35,-735);
        p.teleport(spawn);
        p.sendMessage(ChatColor.GRAY+"==========================");
        p.sendMessage(ChatColor.GREEN+"Welcome to tpbows!");
        p.sendMessage(ChatColor.GOLD+"/tpbow class"+ChatColor.GREEN+" to choose your class!");
        p.sendMessage(ChatColor.GOLD+"/sb"+ChatColor.GREEN+" to join the scoreboard!");
        p.sendMessage(ChatColor.GREEN+"Use your "+ChatColor.GOLD+"teleportBow"+ChatColor.GREEN+" to teleport");
        p.sendMessage(ChatColor.GREEN+"First to "+ChatColor.DARK_PURPLE+"30 kills"+ChatColor.GREEN+" wins");
        p.sendMessage(ChatColor.GRAY+"=====Made by Asilthar=====");
        clearInventoryClass.clearInventory(p);
        loginItem.giveLoginItem(p);
    }
}
