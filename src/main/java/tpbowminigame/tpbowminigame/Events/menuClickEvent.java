package tpbowminigame.tpbowminigame.Events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tpbowminigame.tpbowminigame.Utils.clearInventoryClass;
import tpbowminigame.tpbowminigame.Utils.giveTpbow;
import tpbowminigame.tpbowminigame.Utils.guiItemCreator;
import tpbowminigame.tpbowminigame.Utils.loginItem;

import java.util.ArrayList;

public class menuClickEvent implements Listener {
    @EventHandler
    public void onMenuClick(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();

            if (e.getView().getTitle().equalsIgnoreCase(ChatColor.GOLD + "Choose your class")) {
                e.setCancelled(true);
                switch (e.getCurrentItem().getType()) {
                    case FEATHER:
                        clearInventoryClass.clearInventory(p);

                        loginItem.giveLoginItem(p);
                        ItemStack elytra = guiItemCreator.guiItemCreatorMethod(Material.ELYTRA, ChatColor.LIGHT_PURPLE + "Elytra", "Fly like a bird");
                        ItemMeta elytra_meta = elytra.getItemMeta();
                        elytra_meta.setUnbreakable(true);
                        elytra.setItemMeta(elytra_meta);

                        ItemStack crossbow = new ItemStack(Material.CROSSBOW);
                        ItemMeta crossbow_meta = crossbow.getItemMeta();
                        crossbow_meta.setDisplayName(ChatColor.GOLD + "Crossbow");
                        crossbow_meta.setUnbreakable(true);
                        crossbow_meta.addEnchant(Enchantment.QUICK_CHARGE, 1, false);
                        crossbow_meta.addEnchant(Enchantment.PIERCING, 4, false);
                        crossbow_meta.addEnchant(Enchantment.MULTISHOT, 1, false);
                        crossbow.setItemMeta(crossbow_meta);
                        crossbow.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 100);
                        p.getInventory().addItem(crossbow);
                        giveTpbow.giveDefaults(p);
                        p.getInventory().addItem(elytra);
                        ItemStack arrow = new ItemStack(Material.ARROW);
                        p.getInventory().addItem(arrow);
                        p.closeInventory();
                        break;
                    case DIAMOND_AXE:
                        clearInventoryClass.clearInventory(p);

                        loginItem.giveLoginItem(p);
                        ItemStack diamond_axe = guiItemCreator.guiItemCreatorMethod(Material.DIAMOND_AXE,
                                ChatColor.RED + "Mighty Axe",
                                "Only the strongest vikings are able to wield this weapon.");
                        ItemMeta diamond_axe_meta = diamond_axe.getItemMeta();
                        diamond_axe_meta.setUnbreakable(true);
                        diamond_axe.setItemMeta(diamond_axe_meta);
                        diamond_axe.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 5);
                        p.getInventory().addItem(diamond_axe);
                        giveTpbow.giveDefaults(p);
                        ItemStack swiftness_potion = new ItemStack(Material.POTION);
                        PotionMeta swiftness_potion_meta = (PotionMeta) swiftness_potion.getItemMeta();
                        swiftness_potion_meta.setDisplayName(ChatColor.GREEN + "Gotta go fast!");
                        PotionEffect swiftness = new PotionEffect(PotionEffectType.SPEED, 200, 1);
                        swiftness_potion_meta.addCustomEffect(swiftness, true);
                        swiftness_potion_meta.setColor(Color.GRAY);
                        swiftness_potion.setItemMeta(swiftness_potion_meta);
                        p.getInventory().addItem(swiftness_potion);
                        p.closeInventory();
                        break;
                    case DIAMOND_SWORD:
                        clearInventoryClass.clearInventory(p);

                        loginItem.giveLoginItem(p);
                        ItemStack diamond_sword = guiItemCreator.guiItemCreatorMethod(Material.DIAMOND_SWORD,
                                ChatColor.DARK_PURPLE + "Shadow's Death",
                                "Only the most skilful duelists can weild this sword");
                        ItemMeta diamond_sword_meta = diamond_sword.getItemMeta();
                        diamond_sword_meta.setUnbreakable(true);
                        diamond_sword_meta.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
                        diamond_sword_meta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
                        diamond_sword.setItemMeta(diamond_sword_meta);
                        p.getInventory().addItem(diamond_sword);
                        giveTpbow.giveDefaults(p);

                        ItemStack potion_of_invisibility = new ItemStack(Material.POTION);
                        PotionMeta potion_meta = (PotionMeta) potion_of_invisibility.getItemMeta();
                        potion_meta.setDisplayName("Become one with the shadows");
                        ArrayList<String> potion_lore = new ArrayList<>();
                        potion_lore.add(ChatColor.GOLD + "Turns you invisible until you attack");
                        PotionEffect invisibility = new PotionEffect(PotionEffectType.INVISIBILITY, 600, 1);
                        potion_meta.addCustomEffect(invisibility, true);
                        potion_meta.setColor(Color.SILVER);
                        potion_meta.setLore(potion_lore);
                        potion_of_invisibility.setItemMeta(potion_meta);
                        p.getInventory().addItem(potion_of_invisibility);

                        ItemStack potion_of_blindness = new ItemStack(Material.POTION);
                        PotionMeta blindness_meta = (PotionMeta) potion_of_blindness.getItemMeta();
                        blindness_meta.setDisplayName("Blindness poison");
                        ArrayList<String> blindness_lore = new ArrayList<>();
                        blindness_lore.add(ChatColor.GOLD + "Blinds the victim of your next melee for 1 second");

                        blindness_meta.setColor(Color.BLACK);
                        blindness_meta.setLore(potion_lore);
                        potion_of_blindness.setItemMeta(blindness_meta);
                        p.getInventory().addItem(potion_of_blindness);

                        p.closeInventory();
                        break;
                    case TRIDENT:
                        clearInventoryClass.clearInventory(p);
                        loginItem.giveLoginItem(p);
                        ItemStack trident = new ItemStack(Material.TRIDENT);
                        ItemMeta trident_meta = trident.getItemMeta();
                        trident_meta.setDisplayName(ChatColor.DARK_AQUA+"Poseidon's Rule");
                        ArrayList<String> trident_lore = new ArrayList<>();
                        trident_lore.add(ChatColor.AQUA+"A special trident which once belonged to a powerful god.");
                        trident_meta.setLore(trident_lore);
                        trident_meta.setUnbreakable(true);
                        trident_meta.addEnchant(Enchantment.RIPTIDE,3,true);
                        trident_meta.addEnchant(Enchantment.LOYALTY,3,true);
                        trident_meta.addEnchant(Enchantment.IMPALING,5,true);
                        trident.setItemMeta(trident_meta);
                        p.getInventory().addItem(trident);


                            if(false) {
                                ItemStack rod = new ItemStack(Material.BLAZE_ROD);
                                ItemMeta rod_meta = rod.getItemMeta();
                                rod_meta.setDisplayName(ChatColor.GOLD + "Zeus' Caller");
                                ArrayList<String> rod_lore = new ArrayList<>();
                                rod_lore.add(ChatColor.LIGHT_PURPLE + "The rod of Zeus is capable of instantly summoning lighting");
                                rod_meta.setLore(rod_lore);
                                rod_meta.addEnchant(Enchantment.MENDING, 1, true);
                                rod_meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                                rod.setItemMeta(rod_meta);
                                p.getInventory().addItem(rod);
                                p.getInventory().addItem(new ItemStack(Material.COOKED_PORKCHOP));

                        }
                        p.closeInventory();
                        break;
                }






            }


        }

    }


