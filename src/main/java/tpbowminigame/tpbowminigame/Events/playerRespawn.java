package tpbowminigame.tpbowminigame.Events;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.Random;

public class playerRespawn implements Listener {
    Random rand = new Random();
    Location spawn;

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e){
        Player p = e.getPlayer();
        World world = p.getWorld();
        int rand_int = rand.nextInt(4);
        switch(rand_int){
            case 0:
                spawn = new Location(world,328 ,31, -784);
                e.setRespawnLocation(spawn);
                break;
            case 1:
                spawn = new Location(world,341 ,39, -735);
                e.setRespawnLocation(spawn);
                break;
            case 2:
                spawn = new Location(world,386 ,34, -689);
                e.setRespawnLocation(spawn);
                break;
            case 3:
                spawn = new Location(world, 295,34, -691);
                e.setRespawnLocation(spawn);
                break;


        }

    }
}
