package tpbowminigame.tpbowminigame.Events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.inventory.ItemStack;

public class foodEat implements Listener {
    @EventHandler
    public void onFoodEatEvent(FoodLevelChangeEvent e){
        Player p = (Player) e.getEntity();
        ItemStack food = e.getItem();
        if (food != null){
            e.setCancelled(true);
            int food_level = p.getFoodLevel();
            p.setFoodLevel(food_level+8);
            p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));

        }

    }
}
