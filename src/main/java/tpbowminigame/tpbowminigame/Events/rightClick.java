package tpbowminigame.tpbowminigame.Events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import tpbowminigame.tpbowminigame.Tpbowminigame;
import tpbowminigame.tpbowminigame.Utils.GUImanager;

import java.util.Objects;

import static org.bukkit.event.block.Action.RIGHT_CLICK_AIR;
import static org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK;

public class rightClick implements Listener {
    Tpbowminigame plugin;

    public rightClick(Tpbowminigame plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onRightClick (PlayerInteractEvent e){
        if (e.getAction().equals(RIGHT_CLICK_AIR) || e.getAction().equals(RIGHT_CLICK_BLOCK)){
            Player p = e.getPlayer();
            if (Objects.requireNonNull(p.getInventory().getItemInMainHand().getItemMeta()).getDisplayName().equalsIgnoreCase(ChatColor.GOLD+"Choose class")){
                GUImanager.chooseClassMenu(p);
            } else if(p.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_AQUA+"Poseidon's Rule")){
                Location loc = p.getLocation();
                loc.setY(loc.getBlockY()+1);
                if (loc.getBlock().getType().equals(Material.AIR))
                {
                        loc.getBlock().setType(Material.WATER);
                        BukkitTask clear_water_task = new clearWaterTask(plugin, loc).runTaskLater(plugin, 100);
                }

            }

        }

    }
}
