package tpbowminigame.tpbowminigame.Events;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import tpbowminigame.tpbowminigame.Tpbowminigame;

public class teleportbowevent implements Listener {

    Tpbowminigame plugin;

    public teleportbowevent(Tpbowminigame plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onlandevent(ProjectileHitEvent e) {
        if (e.getEntity() instanceof Arrow) {
            if (e.getEntity().getShooter() instanceof Player) {
                Player player = (Player) e.getEntity().getShooter();
                if (player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Crossbow")) {
                    ItemStack arrow = new ItemStack(Material.ARROW);
                    player.getInventory().addItem(arrow);
                } else {
                    Location land = e.getEntity().getLocation();
                    Float yaw = player.getLocation().getYaw();
                    Float pitch = player.getLocation().getPitch();
                    land.setYaw(yaw);
                    land.setPitch(pitch);
                    player.teleport(land);
                    player.playSound(land, Sound.ENTITY_ENDERMAN_TELEPORT, 1, 1);
                }
            }
        }
    }

        }

