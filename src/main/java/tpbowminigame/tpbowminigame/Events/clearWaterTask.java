package tpbowminigame.tpbowminigame.Events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;
import tpbowminigame.tpbowminigame.Tpbowminigame;

public class clearWaterTask extends BukkitRunnable {
    Tpbowminigame plugin;
    Location loc;

    public clearWaterTask(Tpbowminigame plugin, Location loc) {

        this.plugin = plugin;
        this.loc = loc;
    }

    @Override
    public void run() {
        loc.getBlock().setType(Material.AIR);
    }
}
