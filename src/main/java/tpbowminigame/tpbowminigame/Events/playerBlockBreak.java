package tpbowminigame.tpbowminigame.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import tpbowminigame.tpbowminigame.Tpbowminigame;

public class playerBlockBreak implements Listener {
    Tpbowminigame plugin;

    public playerBlockBreak(Tpbowminigame plugin) {
        this.plugin = plugin;
    }
    @EventHandler
    public void onPlayerBlockBreak(BlockBreakEvent e){
        Player p = e.getPlayer();
        if (!p.hasPermission("tpbows.builder")) {
            e.setCancelled(plugin.getConfig().getBoolean("no-block-breaking"));
        }
    }
}
