package tpbowminigame.tpbowminigame.Events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class playerDamage implements Listener {
    PotionEffect invisibility = new PotionEffect(PotionEffectType.INVISIBILITY,400,1);
    PotionEffect fake_effect = new PotionEffect(PotionEffectType.UNLUCK, 400, 1);

    @EventHandler
    public void onPlayerDamage (EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player){
            Player attacker = (Player) e.getDamager();

            for (PotionEffect effect : attacker.getActivePotionEffects()){
                if (effect.getType().equals(invisibility.getType())){
                    attacker.removePotionEffect(PotionEffectType.INVISIBILITY);
                }
                if (effect.getType().equals(fake_effect.getType())){
                    if (e.getEntity() instanceof Player){
                        Player victim = (Player) e.getEntity();
                        Bukkit.broadcastMessage("Victim: "+victim.getDisplayName());
                        PotionEffect blindness = new PotionEffect(PotionEffectType.BLINDNESS,60,1);
                        victim.addPotionEffect(blindness);
                        attacker.removePotionEffect(PotionEffectType.UNLUCK);
                    }

                }

            }
        }
        if (e.getEntity() instanceof Player){
            Player victim = (Player) e.getEntity();
            for (PotionEffect effect : victim.getActivePotionEffects()){
                if (effect.getType().equals(invisibility.getType())){
                    victim.removePotionEffect(PotionEffectType.INVISIBILITY);
                }
                if (effect.getType().equals(fake_effect.getType())){
                    victim.removePotionEffect(PotionEffectType.UNLUCK);
                }
            }
        }
    }
}

