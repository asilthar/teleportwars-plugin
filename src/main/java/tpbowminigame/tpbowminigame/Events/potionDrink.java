package tpbowminigame.tpbowminigame.Events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class potionDrink implements Listener {
    @EventHandler
    public void onPotionDrink(PlayerItemConsumeEvent e){
        Player p = e.getPlayer();
        if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN+"Gotta go fast!")){
            e.setCancelled(true);
            PotionEffect swiftness = new PotionEffect(PotionEffectType.SPEED, 200, 1);
            p.addPotionEffect(swiftness);

        }
        else if ( e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("Become one with the shadows"
        )){
            e.setCancelled(true);
            PotionEffect invisibility = new PotionEffect(PotionEffectType.INVISIBILITY, 400, 1);
            p.addPotionEffect(invisibility);
        }else if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("Blindness poison")){
            e.setCancelled(true);
            PotionEffect fake_effect = new PotionEffect(PotionEffectType.UNLUCK, 400, 1);
            p.addPotionEffect(fake_effect);

        }

    }


}
