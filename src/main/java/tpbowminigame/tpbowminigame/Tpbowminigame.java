package tpbowminigame.tpbowminigame;

import org.bukkit.plugin.java.JavaPlugin;
import tpbowminigame.tpbowminigame.Events.*;
import tpbowminigame.tpbowminigame.commands.givePlayerBow;
import tpbowminigame.tpbowminigame.commands.givePlayerBowTAB;
import tpbowminigame.tpbowminigame.commands.spawn;

public final class Tpbowminigame extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        getConfig().options().copyDefaults();
        saveDefaultConfig();
        getCommand("tpbow").setExecutor(new givePlayerBow());
        getCommand("tpbow").setTabCompleter(new givePlayerBowTAB());
        getServer().getPluginManager().registerEvents(new teleportbowevent(this), this);
        getServer().getPluginManager().registerEvents(new menuClickEvent(),this);
        getServer().getPluginManager().registerEvents(new potionDrink(),this);
        getServer().getPluginManager().registerEvents(new onFallEvent(), this);
        getServer().getPluginManager().registerEvents(new foodEat(),this);
        getServer().getPluginManager().registerEvents(new onLogIn(),this);
        getServer().getPluginManager().registerEvents(new playerRespawn(),this);
        getServer().getPluginManager().registerEvents(new playerDamage(),this);
        getCommand("spawn").setExecutor(new spawn(this));
        getServer().getPluginManager().registerEvents(new playerBlockBreak(this),this);
        getServer().getPluginManager().registerEvents(new rightClick(this),this);
        getServer().getPluginManager().registerEvents(new playerPickupItem(),this);
        getServer().getPluginManager().registerEvents(new liquidFlow(),this);

    }
}
