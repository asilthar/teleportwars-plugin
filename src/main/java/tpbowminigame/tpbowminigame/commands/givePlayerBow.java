package tpbowminigame.tpbowminigame.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;
import tpbowminigame.tpbowminigame.Utils.GUImanager;
import tpbowminigame.tpbowminigame.Utils.giveTpbow;

public class givePlayerBow implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {


        if (sender instanceof Player){
            Player p = (Player) sender;
            if(args.length == 1&& args[0].equals("class")){
                GUImanager.chooseClassMenu(p);
                p.removePotionEffect(PotionEffectType.INVISIBILITY);
            }
            if(args.length == 0){
                p.sendMessage("Usage:");
                p.sendMessage("/tpbow class "+ ChatColor.GREEN+"Choose your class");
            }

        }

        return true;
    }
}
