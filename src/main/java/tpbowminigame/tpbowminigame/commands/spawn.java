package tpbowminigame.tpbowminigame.commands;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tpbowminigame.tpbowminigame.Tpbowminigame;

public class spawn implements CommandExecutor {
    Tpbowminigame plugin;

    public spawn(Tpbowminigame plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player){
            Player p = (Player) sender;
            World world = p.getWorld();
            int x = plugin.getConfig().getInt("spawn.x");
            int y = plugin.getConfig().getInt("spawn.y");
            int z = plugin.getConfig().getInt("spawn.z");
            Location spawnpoint = new Location(world,x,y,z);
            p.teleport(spawnpoint);
        }


        return true;
    }
}
