package tpbowminigame.tpbowminigame.Utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class guiItemCreator {
    public static ItemStack guiItemCreatorMethod(Material material, String name, String lore){
        ItemStack item = new ItemStack(material);
        ItemMeta item_meta = item.getItemMeta();
        item_meta.setDisplayName(name);
        ArrayList<String> item_lore = new ArrayList<>();
        item_lore.add(lore);
        item_meta.setLore(item_lore);
        item.setItemMeta(item_meta);
        return item;
    }
}
