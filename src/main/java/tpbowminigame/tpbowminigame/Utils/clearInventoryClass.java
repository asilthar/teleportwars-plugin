package tpbowminigame.tpbowminigame.Utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class clearInventoryClass {

    public static void clearInventory(Player p){
        p.getInventory().clear();
        p.getInventory().setHelmet(null);
        p.getInventory().setChestplate(null);
        p.getInventory().setLeggings(null);
        p.getInventory().setBoots(null);

    }
}
