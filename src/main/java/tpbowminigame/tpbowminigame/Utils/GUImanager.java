package tpbowminigame.tpbowminigame.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class GUImanager {

    public static void chooseClassMenu(Player p){
        Inventory choose_menu = Bukkit.createInventory(p,27, ChatColor.GOLD+"Choose your class");

        ItemStack hawk_class = guiItemCreator.guiItemCreatorMethod
                (Material.FEATHER,
                        ChatColor.DARK_RED+"Hawk",
                        "The most mobile class. Items: Elytra, Crossbow");
        ItemMeta hawk_class_meta = hawk_class.getItemMeta();
        ArrayList<String> hawk_lore = new ArrayList<>();
        hawk_lore.add(ChatColor.DARK_AQUA+"Use your high mobility to execute flying maneuvers and deal high burst damage with your crossbow. Remember the holding the crossbow cancels teleports!");
        hawk_lore.add(ChatColor.GOLD+"Items: "+ChatColor.RED+"Elytra, Crossbow");
        hawk_class_meta.setLore(hawk_lore);
        hawk_class_meta.addEnchant(Enchantment.MENDING,1,true);
        hawk_class_meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        hawk_class.setItemMeta(hawk_class_meta);
        choose_menu.setItem(9,hawk_class);

        ItemStack viking_class = guiItemCreator.guiItemCreatorMethod
                (Material.DIAMOND_AXE,
                        ChatColor.DARK_RED+"Viking",
                        "Highest damage but melee secondary. Items: Axe, Potion of swiftness");
        ItemMeta viking_class_meta = viking_class.getItemMeta();
        ArrayList<String> viking_lore = new ArrayList<>();
        viking_lore.add(ChatColor.DARK_AQUA+"Highest damage class, use your speed to catch up to other players. Destroy everyone in close quarters.");
        viking_lore.add(ChatColor.GOLD+"Items: "+ChatColor.RED+"Axe, Potion of swiftness");
        viking_class_meta.setLore(viking_lore);
        viking_class_meta.addEnchant(Enchantment.MENDING,1,true);
        viking_class_meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        viking_class_meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        viking_class.setItemMeta(viking_class_meta);
        choose_menu.setItem(10,viking_class);

        ItemStack assassin_class = guiItemCreator.guiItemCreatorMethod
                (Material.DIAMOND_SWORD,
                ChatColor.DARK_RED+"Assassin",
                ChatColor.DARK_AQUA+"Sneak in the shadows and execute your enemies with poison and a high damage sword.");
        ItemMeta assassin_class_meta = assassin_class.getItemMeta();
        ArrayList<String> assassin_lore = new ArrayList<>();
        assassin_lore.add(ChatColor.DARK_AQUA+"Execute your enemies with a high damage sword. Your potion grants you invisibility until you attack");
        assassin_lore.add(ChatColor.GOLD+"Items: "+ChatColor.RED+"Sword, Potion of invisibility, Potion of burst damage");
        assassin_class_meta.setLore(assassin_lore);
        assassin_class_meta.addEnchant(Enchantment.MENDING,1,true);
        assassin_class_meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        assassin_class_meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        assassin_class.setItemMeta(assassin_class_meta);
        choose_menu.setItem(11,assassin_class);

        ItemStack olympian_class = guiItemCreator.guiItemCreatorMethod(Material.TRIDENT,ChatColor.DARK_RED+"Olympian"+ChatColor.RED+" (In development)",
                ChatColor.DARK_RED+"Use the power of gods to defeat your opponents");
        ItemMeta olympian_class_meta = olympian_class.getItemMeta();
        ArrayList<String> olympian_lore = new ArrayList<>();
        olympian_lore.add(ChatColor.DARK_AQUA+"Olympian lore");
        olympian_lore.add(ChatColor.GOLD+"Items: "+ChatColor.RED+"Trident with constant riptide, Rod of Zeus");
        olympian_class_meta.setLore(olympian_lore);
        olympian_class_meta.addEnchant(Enchantment.MENDING,1,true);
        olympian_class_meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        olympian_class_meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        olympian_class.setItemMeta(olympian_class_meta);
        choose_menu.setItem(12,olympian_class);

        ItemStack mage_class = guiItemCreator.guiItemCreatorMethod
                (Material.SPLASH_POTION,
                        ChatColor.DARK_RED+"Brewer"+ChatColor.RED+" (In development)",
                        ChatColor.DARK_AQUA+"Use your potions for defence and offence");
        ItemMeta mage_meta = mage_class.getItemMeta();
        ArrayList<String> mage_lore = new ArrayList<>();
        mage_lore.add(ChatColor.DARK_AQUA+"Mage lore");
        mage_lore.add(ChatColor.GOLD+"Items: "+ChatColor.RED+"Splash Potion of poison, Splash Potion of harming, Splash Potion of healing");
        mage_meta.setLore(mage_lore);
        mage_meta.addEnchant(Enchantment.MENDING,1,true);
        mage_meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        mage_meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        mage_class.setItemMeta(mage_meta);
        choose_menu.setItem(13,mage_class);




    p.openInventory(choose_menu);
    }





}
