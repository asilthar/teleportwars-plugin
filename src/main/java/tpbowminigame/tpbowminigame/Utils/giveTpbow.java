package tpbowminigame.tpbowminigame.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class giveTpbow {

    public static void giveDefaults(Player p){
        ItemStack tpbow = new ItemStack(Material.BOW,1);
        ItemMeta tpbow_meta = tpbow.getItemMeta();
        tpbow_meta.setUnbreakable(true);
        tpbow_meta.setDisplayName(ChatColor.LIGHT_PURPLE+"Teleport Bow");
        ArrayList<String> tpbow_lore = new ArrayList<>();
        tpbow_lore.add(ChatColor.DARK_RED+"Holding this bow will make your arrows teleport you!");
        tpbow_meta.setLore(tpbow_lore);
        tpbow.setItemMeta(tpbow_meta);
        tpbow.addUnsafeEnchantment(Enchantment.ARROW_INFINITE,100);
        p.getInventory().addItem(tpbow);
        ItemStack arrow = new ItemStack(Material.ARROW);
        p.getInventory().setItem(9,arrow);
        p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
    }
}
