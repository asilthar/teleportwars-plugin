package tpbowminigame.tpbowminigame.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class loginItem {

    public static void giveLoginItem(Player p){
        ItemStack loginItem = new ItemStack(Material.COMPASS, 1);
        ItemMeta loginItem_meta = loginItem.getItemMeta();
        loginItem_meta.setDisplayName(ChatColor.GOLD+"Choose class");
        loginItem_meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        loginItem.setItemMeta(loginItem_meta);
        p.getInventory().setItem(8,loginItem);
    }


}
